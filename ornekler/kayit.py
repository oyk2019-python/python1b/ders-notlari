kullanicilar = []
secim = None

while secim != 3:
    print("1. Kullanıcı Ekle")
    print("2. Kullanıcıları Listele")
    print("3. Çıkış")

    secim = int(input("Seçim Yapınız: "))

    if secim == 1:
        kullanici_adi = input("İsim giriniz: ")
        pin = int(input("PIN giriniz: "))

        # kullanici = {
        #     'username': kullanici_adi,
        #     'PIN': pin,
        # }
        kullanici = dict(
            username=kullanici_adi,
            PIN=pin,
        )
        # kullanici['username'] = kullanici_adi
        # kullanici['pin'] = pin

        # kullanici[username] = pin
        kullanicilar.append(kullanici)
    elif secim == 2:
        if len(kullanicilar) == 0:
            print("Gösterilecek kullanıcı yok.")
        else:
            for kullanici in kullanicilar:
               print(kullanici["username"])
