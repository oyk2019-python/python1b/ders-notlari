class Kume(list):
    def __init__(self, liste, *args, **kwargs):
        yeni_liste = []
        for oge in liste:
            if oge not in yeni_liste:
                yeni_liste.append(oge)
        super().__init__(yeni_liste, *args, **kwargs)

    def append(self, value):
        if value in self:
            return
        super().append(value)


a_kumesi = Kume([10, 10, 10, 20, 20, 30, 30])
a_kumesi.append(100)
a_kumesi.append(200)
a_kumesi.append(100)

print(a_kumesi)

class BenimListem(list):
    def append(self, value):
        print("YEĞEĞYEĞ")
        print(self)
        super().append(value)
        print("HELO")
        print(self)

# liste = BenimListem()
# liste.append(10)
# liste.append(100)

# print(liste)
