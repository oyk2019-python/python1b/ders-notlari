sayilar = []

def sayi_ekle(eklenecek_liste, sayi):
    eklenecek_liste.append(sayi)
    return eklenecek_liste

def ortalama_al(*args):
    if len(args) > 0:
        toplam = 0
        for sayi in args:
            toplam = toplam + sayi
        return toplam / len(args)
    else:
        return 0

while True:
    print("""
    1. Yeni sayi ekle
    2. Ortalama Al
    3. Listeyi temizle
    4. Cikis
    """)

    secim = int(input("Secim yapiniz: "))

    if secim == 1:
        alinan_sayi = int(input("Sayi giriniz: "))
        sayi_ekle(sayilar, alinan_sayi)
    elif secim == 2:
        hesaplanan = ortalama_al(*sayilar)
        print("Sayilarin ortalamasi: {}".format(
            hesaplanan))
    elif secim == 3:
        sayilar = []
    elif secim == 4:
        break
    else:
        print("Hatali secim!")
