import random

class Zar:
    def __init__(self, kose):
        self.kose = kose

    def at(self):
        return random.randint(1, self.kose)


class HileliZar(Zar):
    def at(self):
        sayi = 0
        while sayi < self.kose / 2:
            sayi = super().at()
        return sayi

yeni_zar = Zar(100)
print(yeni_zar.at())

hileli = HileliZar(100)
print(hileli.at())
